from binaryConverter import decToBinList
import pytest


def test_normal_case():
    input = 25
    expected_output = [1, 1, 0, 0, 1]
    assert decToBinList(input) == expected_output

def test_powers_of_two():
    input = 8
    expected_output = [1, 0, 0, 0]
    assert decToBinList(input) == expected_output

def test_zero():
    input = 0
    expected_output = [0]
    assert decToBinList(input) == expected_output
