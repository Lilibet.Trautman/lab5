from binaryConverter import binToDecList
import pytest

def test_generic_case():
    input = [1, 1, 0, 0, 1]
    expected_output = 25
    assert binToDecList(input) == expected_output

